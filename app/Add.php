<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Add extends Model
{
    
    use Notifiable;

   
    protected $fillable = [
        'first_name', 'last_name', 'email','job_title','phone','place','image',
    ];

    
}
