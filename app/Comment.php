<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Comment extends Model
{
    use Notifiable;
    protected $guarded = [];

   protected $table = 'comments';

   
    protected $fillable = [
         'add_id','schedule_id','parent_id', 'body',
    ];

      public function add()
    {
        return $this->belongsTo(Add::class);
    }

    //  public function schedule()
    // {
    //     return $this->belongsTo(Schedule::class);
    // } 

    public function replies()
    {
        return $this->hasMany(Comment::class, 'parent_id');
    }

}
