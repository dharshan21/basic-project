<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

use Mail;

use App\Text;

use App\Schedule;

class MinuteUpdate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'Minute:Update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        // $contacts = Contact::all();
        // foreach ($contacts as $contact) {
            
        //     Mail::raw("email send the message", function($message) use ($contact)
        //     {
        //         $message->from('prasanthmca1998@gmail.com');
        //         $message->to($contact->email)->subject('Hour Update');
        //     });
        // }
        //  $this->info('send Successfully!');

     //    $now=date("Y-m-d H:i:s");
     //    $data=DB::table('text')->where('show_',0)->whereRaw("date(posted_at)<='$now'")->get();
     //    $data->each(function ($item){
     //    DB::table('text')->where('id',$item->id)->update(['show_'=>1]);
     // // });

        // DB::table("texts")->update(["posted_at"=>date("Y-m-d h:i:s")]);
        // $texts = Text::where('title', 'planned')->where('posted_at', '<', now())->get();
         
        $texts = Text::all();

        foreach($texts as $text) {
            $id=$text->id;
            $title=$text->title;
            $body=$text->body;
            $image=$text->image;
            $posted_at=$text->posted_at;
            $created=$text->created_at;
            $updated=$text->updated_at;

            $date = date_create($posted_at);

           if( date_format($date, 'Y-m-d H:i') == date('Y-m-d H:i') )
             {
                
               DB::table('schedules')->insertOrIgnore([
                    [ 
                        'id' => $id,
                        'title' => $title,
                        'body' => $body,
                        'image' => $image,
                        'created_at' => $created,
                        'updated_at' => $updated,

                    ]


                ]);

            }

            
            else
            {

                echo "No Schdule Value";
            }
           

        }
        
       
    }

}
