<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use App\Text;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
          Commands\MinuteUpdate::class,

        
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
     {
        $schedule->command('Minute:Update')->everyMinute();

        // $schedule->call(function () {
        //     DB::table('texts')->([
        //         ["title" => $response, "created_at” => “$published_at"],
     
        //     ])->everyMinute();
        //  });
        // $texts = Text::all();
  
        // foreach ($texts as $text) {
  
        //     $posted_at = $text->posted_at;
  
        //     $schedule->call(function() use($text) {
            
        //         Log::info($text->title.' '.\Carbon\Carbon::now());
        //     });

        }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
