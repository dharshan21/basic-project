<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Contact extends Model
{
     use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'user_id',
    ];

    // public function user(){
    // 	return $this->belongTo('user');
    // }

    //  public function user(){
    //     return $this->belongsToMany('App\User','contact_user');
    // }

     public function user(){
    
       return $this->belongsTo(User::class,'user_id');
   }

}