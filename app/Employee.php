<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Employee extends Model
{
    public function getEmployee()
    {
    	$records = DB::table('employees')->select('id', 'name', 'email', 'phone', 'department')->get()->toArray();

    	return $records;
    }
}
