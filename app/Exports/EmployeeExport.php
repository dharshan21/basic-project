<?php

namespace App\Exports;

use App\Employee;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

// use App\Exports\
class EmployeeExport implements FromCollection,WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function headings():array{

    	return [
    		'id',
    		'name',
    		'email',
    		'phone',
    		'department'
    	];

    }


    public function collection()
    {
        // return AppEmployee::all();

        return collect(Employee::all());
    }
}
