<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Add;

class AddController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $adds = Add::all();
        return view('layouts.show',compact('adds'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('layouts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $adds = new Add();

        $adds->first_name = $request->get('first_name');
        $adds->last_name = $request->get('last_name');
        $adds->email = $request->get('email');
        $adds->job_title = $request->get('job_title');
        $adds->phone = $request->get('phone');
        $adds->place = $request->get('place');
        $file_name = time().'.'.$request->image->extension();
        $request->image->move(public_path('myfiles'),$file_name);
        $adds->image = $file_name;
        $adds->save();

       return redirect('blog')->with('success', 'added Successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $add = Add::find($id);
        return view('layouts.view',compact('add'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $add = Add::find($id);
        return view('layouts.edit',compact('add'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $adds = Add::find($id);
        
        $adds->first_name = $request->get('first_name');
        $adds->last_name = $request->get('last_name');
        $adds->email = $request->get('email');
        $adds->job_title = $request->get('job_title');
        $adds->phone = $request->get('phone');
        $adds->place = $request->get('place');
        $file_name = time().'.'.$request->image->extension();
        $request->image->move(public_path('myfiles'),$file_name);
        $adds->image = $file_name;
        $adds->save();
        return redirect('blog')->with('success', 'Update Successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $add = Add::find($id);
        $add ->delete();
        return redirect('blog')->with('success', 'delete Successfully!');
    }
}
