<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Cache;

class CacheController extends Controller
{
    
    public function index()
    {
    	return view('user.index');
    }

    public function search(Request $request)
    {
    	 try {
          $cache = Cache::findOrFail($request->input('user_id'));
        } catch (\Exception $e) {
            return back()->withError($e->getMessage())->withInput();
        }
        return view('user.search', compact('cache'));
    }
}