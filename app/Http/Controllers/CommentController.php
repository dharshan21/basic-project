<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Comment;

use App\Schedule;

use App\Add;

class CommentController extends Controller
{
    
    public function store(Request $request )
    {
    	$request->validate ([
    		'body'=>'required',
    	]);
        
    	$input = $request->all();
    	$input['add_id'] = auth()->user()->id;
    
        Comment::create($input);
   
        return back();
    	
    }
      

}
