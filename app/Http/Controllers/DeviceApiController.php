<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
 
use App\Contact;

use Illuminate\Support\Facades\Validator;


class DeviceApiController extends Controller
{

	public function list()          //Api get method
	{
        return Contact::all();
	}

	public function add(Request $request)      //Api post method
	{
		$contact = new Contact([

	    'name' => $request->get('name'),
		'user_id' => $request->get('user_id'),
		'email' => $request->get('email'),

		]);
		
		$result = $contact->save();

		if ($result) {
			return "value is saved";
		}
		else{

			return "unsaved";
		}
		
	}


	public function update(Request $request)      //Api put method
	{

		$contacts = Contact::find($request->id);

	    $contacts->name = $request->get('name');
	    $contacts->email = $request->get('email');
	    $contacts->user_id = $request->get('user_id');

	    $result = $contacts->save();


		if ($result) {
			return "value is saved";
		}
		else{

			return "unsaved";
		}
	}

	public function delete($id)     //Api delete method
	{
		 $contacts = Contact::find($id);
		 $result = $contacts->delete();

		if ($result) {
			return "value is delete";
		}
		else{

			return "undelete";
		}
    }

    public function search($name)
    {
      $result = Contact::where("name", "LIKE", "%" .$name. "%")->get();

      if (count($result)) {
      	 
      	 return $result;
      }
      else{

      	   return "No result";
      }

    }

    public function testData(Request $request)
    {
        $rules =array(
        	"user_id"=>"required|min:2|max:4"
        );
        $validator = Validator::make($request->all(),$rules);

        if ($validator->fails()) 
        {
        	return respone()->json($validator->error(),401);
        }
        else{

            $contact = new Contact;
            $contact->name=$request->name;
            $contact->email=$request->email;
            $contact->user_id=$request->user_id;
		
		    $result = $contact->save();

		     if ($result) {

			return "value is saved";
		     }
		    else{

			return "unsaved";
		    }

           }
    	
    }

    public function upload(Request $request)
    {
    	$result=$request->file('file')->hasFile('doces');
    	return ["result" => $result];
    }

 }