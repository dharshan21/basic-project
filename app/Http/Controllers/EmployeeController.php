<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Employee;

use App\Exports\EmployeeExport;

use Excel;

class EmployeeController extends Controller
{
    //
    public function add()
    {
    	$employees = [
    		["name"=>"dharshan" , "email"=>"dharshan@gmail.com" , "phone"=>"1234567890" , "department"=>"Accounting"],
    		["name"=>"ragul" , "email"=>"ragul@gmail.com" , "phone"=>"1234567880" , "department"=>"Finance"],
    		["name"=>"rithick" , "email"=>"rithick@gmail.com" , "phone"=>"1235567890" , "department"=>"Marketting"],
    		["name"=>"rohith" , "email"=>"rohith@gmail.com" , "phone"=>"1224567890" , "department"=>"Quality"],
    		["name"=>"keerthi" , "email"=>"keerthi@gmail.com" , "phone"=>"1234467890" , "department"=>"Accounting"]
    	];

    	Employee::insert($employees);
    	return "insert successfully";
    	
    }

    public function exportIntoExcel()
    {
    	return Excel::download(new  EmployeeExport,'employeelist.xlsx');
    }

     public function exportIntoCSV()
    {
    	return Excel::download(new  EmployeeExport,'employeelist.csv');
    }
}
