<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\File;

use Redirect;


class FileControllers extends Controller
{
    
    public function index()
    {
      return view('user.file');
    	
    }

    public function show(Request $request)
    {
      request()->validate([
         'file' => 'required',
     ]);
          $input=$request->all();
 
        if($request->hasfile('file'))
         {
 
            foreach($request->file('file') as $file)
            {
                $filename=$file->getClientOriginalName();
                $file->move(public_path().'/uploads/', $filename);  
                $insert['file'] = "$filename";
            }
         }
         
        $check = File::insert($insert);
 
        return Redirect::to("file")
        ->with('success', 'File update Successfully!');
 
    }
}
