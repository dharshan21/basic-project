<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Str;

class HelperController extends Controller
{
    public function helper()
    {
    	$place =str::of('Welcome to trichy')->after('Welcome to');
    	echo $place . '<br>';  


    	$places =str::of('Hello')->append('Welcome!');
    	echo $places . '<br>'; 

    	$places =str::of('SUCCESS')->lower();
    	echo $places . '<br>';

    	$places =str::of('natural in life')->replace('life', 'love');
    	echo $places . '<br>';

    	$title =str::of('natural in life')->title();
    	echo $title . '<br>';

    	$title =str::of('natural in life')->slug('__');
    	echo $title;


    }


    
}

