<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Str;

use App\User;

use Auth;

use Storage;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

     public function handleAdmin()
    {
        return view('admin.home');
    }

    public function edit()
    {
        $user = Auth::user();
        return view('user.edit',compact('user'));
    }

    public function upload(Request $request)
    {
            if($request->hasFile('profile_image')){
            $filename = $request->profile_image->getClientOriginalName();
            $this->deleteOldImage(); 
            $request->profile_image->storeAs('uploads',$filename,'public');
            Auth()->user()->update(['profile_image'=>$filename]);
        }
        return redirect()->back();

       
    }

     protected function deleteOldImage()
    {
      if (auth()->user()->profile_image){
        Storage::delete('/public/uploads/'.auth()->user()->profile_image);
      }
     }

    
}
