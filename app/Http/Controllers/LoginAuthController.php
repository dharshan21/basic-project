<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class LoginAuthController extends Controller
{
    public function sessionLogin(Request $request)
    {
    	$data = $request->input();

    	$request->session()->put('user', $data['user']);

    	return redirect('profile');

    }
}
