<?php

namespace App\Http\Controllers;

use App\resourmodel;
use Illuminate\Http\Request;

class ResourmodelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\resourmodel  $resourmodel
     * @return \Illuminate\Http\Response
     */
    public function show(resourmodel $resourmodel)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\resourmodel  $resourmodel
     * @return \Illuminate\Http\Response
     */
    public function edit(resourmodel $resourmodel)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\resourmodel  $resourmodel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, resourmodel $resourmodel)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\resourmodel  $resourmodel
     * @return \Illuminate\Http\Response
     */
    public function destroy(resourmodel $resourmodel)
    {
        //
    }
}
