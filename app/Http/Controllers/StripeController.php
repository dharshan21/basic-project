<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Stripe; 

use Session; 

class StripeController extends Controller
{
    
    public function paymentIndex()
    {
    	return view('user.home');
    	
    }

    public function paymentStatus(Request $request)
    {
    	Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
        Stripe\Charge::create ([
                "amount" => 100 * 150,
                "currency" => "INR",
                "source" => $request->stripeToken,
                "description" => "Making test payment." 
        ]);
  
        Session::flash('success', 'Payment has been successfully processed.');
          
        return back();
    }
}
