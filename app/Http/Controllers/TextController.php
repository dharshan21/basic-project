<?php

namespace App\Http\Controllers;

use App\Text;
use Illuminate\Http\Request;


class TextController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $texts = Text::all();
        return view('blog.view',compact('texts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('blog.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([

                'title' => 'required|max:200',
                'body' => 'required',
                'image' => 'required|image|mimes:jpeg,png,jpg',


        ]);
                


        $texts = new Text();

        $texts->title = $request->get('title');
        $texts->body = $request->get('body');
        $file_name = time().'.'.$request->image->extension();
        $request->image->move(public_path('myfiles'),$file_name);
        $texts->image = $file_name;
        $texts->posted_at = $request->get('posted_at');
        $texts->save();

       return redirect('texts_add')->with('success', 'added Successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Text  $text
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $text = Text::find($id);
        return view('blog.show',compact('text'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Text  $text
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $text = Text::find($id);
        return view('blog.edit',compact('text'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Text  $text
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,  $id)
    {
        $texts = Text::find($id);
        
        $texts->title = $request->get('title');
        $texts->body = $request->get('body');
        $file_name = time().'.'.$request->image->extension();
        $request->image->move(public_path('myfiles'),$file_name);
        $texts->image = $file_name;
        $texts->save();
        return redirect('texts_add')->with('success', 'Update Successfully!');
    }
    

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Text  $text
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $text = Text::find($id);
        $text->delete();
        return redirect('texts_add')->with('success', 'Delete Successfully!');
    }
}
