<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;


class Schedule extends Model
{
    use Notifiable;

   
    protected $fillable = [
        'title', 'body', 'image',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    //  public function comments()
    // {
    //     return $this->hasMany(Comment::class);
    // }

     public function comments()
    {
        return $this->hasMany(Comment::class)->whereNull('parent_id');
    }
}
