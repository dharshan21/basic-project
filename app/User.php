<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

   
    protected $fillable = [
        'name', 'email', 'password','is_admin','profile_image',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
      
   // public function contact(){
   //      return $this->hasMany('App\Contact');
   //  }


    public function contact() {

        return $this->hasMany(Contact::class, 'user_id');
    }

    public function getImageAttribute(){

        return $this->profile_image;
    }


    // public function contact(){

    //     return $this->hasOne('App\Contact');
    // }


//     public function contact(){
//         return $this->belongsToMany('App\Contact','contact_user');
//     } 
       

//     //Mutators

//     public function setNameAttribute($value){
//         $this->attributes['name'] = ucfirst($value);
//     }
    

//     public function getNameAttribute($value){
//         return strtoupper($value);
//     }
}
