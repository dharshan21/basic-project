<?php

use Illuminate\Database\Seeder;

use App\Blog;

class BlogSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $blogData = [
        	[
        		'name'=>'Admin',
        		'email'=>'admin@gmail.com',
        		'is_admin'=>'1',
        		'password'=>bcrypt('123456'),

        	],

        	[
        		'name'=>'User',
        		'email'=>'user@gmail.com',
        		'is_admin'=>'0',
        		'password'=>bcrypt('123456'),

        	],
        ];
        
        foreach ($blogData as $key => $value) {
        	Blog::create($value);
        }
    }
}
