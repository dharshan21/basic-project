@extends('layouts.app')



@section('content')


<hr>
<form action="{{route('texts_add.update', $text->id)}}" method="post" enctype="multipart/form-data">
@csrf
@method('PATCH')

<div class="container">

    <div class="form-group">
        <label for="title">Title</label>
        <input type="text" class="form-control"  name="title" value="{{ $text->title}}">
    </div>
     <div class="form-group">
        <label for="body">Text</label>
        <textarea type="text" class="form-control" name="body">{{ $text->body}}"</textarea>
    </div>
 
    <div class="form-group">
        <label for="image"> Picture</label>
        <input type="file" class="form-control" name="image">
    </div>


   
    <button type="submit" class="btn btn-primary">Update</button>
    <button type="submit" class="btn btn-primary">Reset</button>

    </div>
</form>

   @endsection