

<ul class="nav nav-pills">
  <li class="nav-item">
    <a class="nav-link active" href="{{'admin/home'}}">Home</a>
  </li>

  <div class="dropdown">
 <button class="btn btn-outline-primary  dropdown-toggle float-right bulk-action-dropdown text-uppercase" href="#" id="dropdownMenuOutlineButton2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    Users Management 
  </button>
  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
    <a class="dropdown-item" href="{{('/blog/create')}}">Add User</a>
    <a class="dropdown-item" href="{{'/blog'}}">View User</a>
  </div>
</div>

  <div class="dropdown">
  <button class="btn btn-outline-primary  dropdown-toggle float-right bulk-action-dropdown text-uppercase" href="#" id="dropdownMenuOutlineButton2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
   Blog Management 
  </button>
  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
    <a class="dropdown-item" href="{{'/blog_add/create'}}">Add Blog</a>
    <a class="dropdown-item" href="{{'/blog_add'}}">View Blog</a>
  </div>
</div>
  
  <li class="nav-item">
    <a class="nav-link disabled" href="#">Disabled</a>
  </li>
</ul>
