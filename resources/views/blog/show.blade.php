@extends('layouts.app')


@section('content')

    <div class="row">
       
        <div class="col-lg-1">
            <a class="btn btn-primary" href="{{ url('texts_add') }}"> Back</a>
        </div>
    </div>
<div class="container">
    <table class="table table-bordered">
        <tr>
            <th>Title:</th>
            <td>{{ $text->title }}</td>
        </tr>
       
        <tr>
            <th>Text:</th>
            <td>{{ $text->body }}</td>
        </tr>
            <th> Picture:</th>
            <td><img src="myfiles/{{ $text->image }}" style="border-radius: 50%" width="80px"></td>       
        </tr>

    </table>
</div>
@endsection