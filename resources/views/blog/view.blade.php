@extends('layouts.app')

@section('content')
@if(session('success'))
 <div class="alert alert-success" role="alert">
 	{{ session('success')}}
 	
 </div>
 @endif
 <a href="{{ route('texts_add.create')}}" class="btn btn-primary">Create </a>
 <ul class="nav nav-pills">
  <li class="nav-item">
    <a class="nav-link" href="{{'/home'}}">Home</a>
  </li>
</ul>
<div class="container">
 <div class="col-sm-12">
 	<div class="row">
 		<table class="table table-striped">
 			<thead>
 				<th>Id</th>
 				<th>Title</th>
 				<th>Text</th>
 				<th>Picture</th>
 				<th>Show</th>
 				<th>Edit</th>
 				<th>Delete</th>
 			</thead>
 			<tbody>
 				@if($texts)
 				@foreach($texts as $text)
 				<tr>
 				<td>{{ $text->id }}</td>
 				<td>{{ $text->title }}</td>
 				<td>{{ $text->body }}</td>
 				<td><img src="myfiles/{{ $text->image }}" style="border-radius: 50%" width="80px"></td>
 				<td> <a class="btn btn-info" href="{{ route('texts_add.show',$text->id) }}">Show</a></td>
 				<td> <a class="btn btn-primary" href="{{ route('texts_add.edit',$text->id) }}">Edit</a></td>
 				<td>
 					<form method="post" action="{{ route('texts_add.destroy',$text->id)}}">
 						@csrf
 						@method('DELETE')
 						<button class="btn btn-danger" type="sumbit">Delete</button>
 					</form>
 				</td>
 				
 			</tr>
 				@endforeach
 				@endif
 			</tbody>
 			
 		</table>
 			
 			
 		</div>
 		
 	</div>
 	
 </div>
</div>
 @endsection