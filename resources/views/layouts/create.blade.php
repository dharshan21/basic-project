@extends('layouts.app')
@extends('layouts.header')

@section('content')


<hr>
<form action="{{route('blog.store')}}" method="post" enctype="multipart/form-data">
@csrf
<div class="container">

    <div class="form-group">
        <label for="title">First_Name</label>
        <input type="text" class="form-control"  name="first_name">
    </div>
     <div class="form-group">
        <label for="title">Last_Name</label>
        <input type="text" class="form-control"  name="last_name">
    </div>
    <div class="form-group">
        <label for="email">Email</label>
         <input type="email" class="form-control" name="email">
     </div> 
     <div class="form-group">
        <label for="job_title">Job_title</label>
        <input type="text" class="form-control" name="job_title">
    </div>
    <div class="form-group">
        <label for="phone">Phone</label>
        <input type="text" class="form-control" name="phone">
    </div>

    <div class="form-group">
        <label for="place">Place</label>
        <input type="text" class="form-control" name="place">
    </div>

    <div class="form-group">
        <label for="image">Profile Picture</label>
        <input type="file" class="form-control" name="image">
    </div>


   
    <button type="submit" class="btn btn-primary">Submit</button>

    </div>
</form>

   @endsection