@extends('layouts.app')
@extends('layouts.header')

@section('content')

<hr>
<form action="{{ route('blog.update',$add->id)}}" method="post" enctype="multipart/form-data">
@csrf
@method('PATCH')

<div class="container">

    <div class="form-group">
        <label for="title">First_Name</label>
        <input type="text" class="form-control"  name="first_name" value="{{$add->first_name}}">
    </div>
     <div class="form-group">
        <label for="title">Last_Name</label>
        <input type="text" class="form-control"  name="last_name" value="{{$add->last_name}}">
    </div>
    <div class="form-group">
        <label for="email">Email</label>
         <input type="email" class="form-control" name="email" value="{{$add->email}}">
     </div> 
     <div class="form-group">
        <label for="job_title">Job_title</label>
        <input type="text" class="form-control" name="job_title" value="{{$add->job_title}}">
    </div>
    <div class="form-group">
        <label for="phone">Phone</label>
        <input type="text" class="form-control" name="phone" value="{{$add->phone}}">
    </div>

    <div class="form-group">
        <label for="place">Place</label>
        <input type="text" class="form-control" name="place" value="{{$add->place}}">
    </div>

    <div class="form-group">
        <label for="image">Profile Picture</label>
        <input type="file" class="form-control" name="image" value="{{$add->image}}">
    </div>

   
    <button type="submit" class="btn btn-primary">Update</button>
    <button type="submit" class="btn btn-light">Reset</button>
</div>
</form>

   @endsection