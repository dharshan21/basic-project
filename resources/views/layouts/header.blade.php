

<ul class="nav nav-pills">
  <li class="nav-item">
    <a class="nav-link active" href="{{'/home'}}">Home</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="{{('/texts_add/create')}}">Schedule Add</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="{{('/texts_add')}}">View Schedule</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="{{('/blog_add/create')}}">Blog</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="{{('/blog_add')}}">View Blogs</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="{{('blog/create')}}">Add User</a>
  </li>
   <li class="nav-item">
    <a class="nav-link" href="{{('/blog')}}">View Users</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="{{('/admin/home' )}}">Back Admin</a>
  </li>
  <li class="nav-item">
    <a class="nav-link disabled" href="#">Disabled</a>
  </li>
</ul>
