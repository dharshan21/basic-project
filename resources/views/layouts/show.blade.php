@extends('layouts.app')
@extends('layouts.header')
@section('content')
@if(session('success'))
 <div class="alert alert-success" role="alert">
 	{{ session('success')}}
 	
 </div>
 @endif
 <a href="{{ route('blog.create')}}" class="btn btn-primary">Create </a>
<div class="container">
 <div class="col-sm-12">
 	<div class="row">
 		<table class="table table-striped">
 			<thead>
 				<th>Id</th>
 				<th>First_Name</th>
 				<th>Last_Name</th>
 				<th>Email</th>
 				<th>Job_title</th>
 				<th>Phone</th>
 				<th>Place</th>
 				<th>Profile Picture</th>
 				<th>Show</th>
 				<th>Edit</th>
 				<th>Delete</th>
 			</thead>
 			<tbody>
 				@if($adds)
 				@foreach($adds as $add)
 				<tr>
 				<td>{{ $add->id }}</td>
 				<td>{{ $add->first_name }}</td>
 				<td>{{ $add->last_name }}</td>
 				<td>{{ $add->email }}</td>
 				<td>{{ $add->job_title }}</td>
 				<td>{{ $add->phone }}</td>
 				<td>{{ $add->place }}</td>
 				<td><img src="myfiles/{{ $add->image }}" style="border-radius: 50%" width="80px"></td>
 				<td> <a class="btn btn-info" href="{{ route('blog.show',$add->id) }}">Show</a></td>
 				<td> <a class="btn btn-primary" href="{{ route('blog.edit',$add->id) }}">Edit</a></td>
 				<td>
 					<form method="post" action="{{ route('blog.destroy',$add->id)}}">
 						@csrf
 						@method('DELETE')
 						<button class="btn btn-danger" type="sumbit">Delete</button>
 					</form>
 				</td>
 				
 			</tr>
 				@endforeach
 				@endif
 			</tbody>
 			
 		</table>
 			
 			
 		</div>
 		
 	</div>
 	
 </div>
</div>
 @endsection