@extends('layouts.app')
@extends('layouts.header')

@section('content')

    <div class="row">
       
        <div class="col-lg-1">
            <a class="btn btn-primary" href="{{ url('blog') }}"> Back</a>
        </div>
    </div>
<div class="container">
    <table class="table table-bordered">
        <tr>
            <th>First_Name:</th>
            <td>{{ $add->first_name }}</td>
        </tr>
        <tr>
            <th>Last_Name:</th>
            <td>{{ $add->last_name }}</td>
        </tr>
        <tr>
            <th>Email:</th>
            <td>{{ $add->email }}</td>
        </tr>
        <tr>
            <th>Job_title:</th>
            <td>{{ $add->job_title }}</td>
        </tr>
        <tr>
            <th>Phone:</th>
            <td>{{ $add->phone }}</td>
        </tr>
        <tr>
            <th>Place:</th>
            <td>{{ $add->place }}</td>
        </tr>
        <tr>
            <th>Profile Picture:</th>
            <td><img src="myfiles/{{ $add->image }}" style="border-radius: 50%" width="80px"></td>       
        </tr>

    </table>
</div>
@endsection