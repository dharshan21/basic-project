@extends('layout.app')

@section('title', 'crud-page')

@section('content')

<h2> New-Crud-Add</h2>

<hr>
<form action="{{route('users.store')}}" method="post">
@csrf
    <div class="form-group">
        <label for="title">Name</label>
        <input type="text" class="form-control"  name="name">
    </div>
    <div class="form-group">
        <label for="email">Email</label>
         <input type="email" class="form-control" name="email">
     </div> 
     <div class="form-group">
        <label for="password">Password</label>
        <input type="password" class="form-control" name="password">
    </div>

    <div class="form-group">
        <label for="address">Address</label>
        <input type="text" class="form-control" name="address">
    </div>

   
    <button type="submit" class="btn btn-primary">Submit</button>
</form>

   @endsection