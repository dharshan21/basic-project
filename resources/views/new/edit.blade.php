
@extends('layout.app')
@section('content')
<h1>Edit Task</h1>
<hr>
<form action="{{ route('users.update',$user->id)}}" method="post">
@csrf
@method('PATCH');
    
   <div class="form-group">
        <label for="title">Name</label>
        <input type="text" class="form-control"  name="name" value="{{$user->name}}">
    </div>
    <div class="form-group">
        <label for="email">Email</label>
         <input type="email" class="form-control" name="email"  value="{{$user->email}}">
     </div> 
     <div class="form-group">
        <label for="password">Password</label>
        <input type="password" class="form-control" name="password"  value="{{$user->password}}">
    </div>

    <div class="form-group">
        <label for="address">Address</label>
        <input type="text" class="form-control" name="address"  value="{{$user->address}}">
    </div>

    <button type="submit" class="btn btn-primary">Update</button>
    <button type="submit" class="btn btn-light">Reset</button>

</form>
@endsection