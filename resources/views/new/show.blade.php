@extends('layouts.app')

@section('content')
@if(session('success'))
 <div class="alert alert-success" role="alert">
 	{{ session('success')}}
 	
 </div>
 @endif
 <a href="{{ route('users.create')}}" class="btn btn-primary">Create </a>

 <div class="col-sm-12">
 	<div class="row">
 		<table class="table table-striped">
 			<thead>
 				<th>Id</th>
 				<th>Name</th>
 				<th>Email</th>
 				<th>Password</th>
 				<th>Address</th>
 				<th>Show</th>
 				<th>Edit</th>
 				<th>Delete</th>
 			</thead>
 			<tbody>
 				@if($users)
 				@foreach($users as $user)
 				<tr>
 				<td>{{ $user->id }}</td>
 				<td>{{ $user->name }}</td>
 				<td>{{ $user->email }}</td>
 				<td>{{ $user->password }}</td>
 				<td>{{ $user->address }}</td>
 				<td> <a class="btn btn-info" href="{{ route('users.show',$user->id) }}">Show</a></td>
 				<td> <a class="btn btn-primary" href="{{ route('users.edit',$user->id) }}">Edit</a></td>
 				<td>
 					<form method="post" action="{{ route('users.destroy',$user->id)}}">
 						@csrf
 						@method('DELETE')
 						<button class="btn btn-danger" type="sumbit">Delete</button>
 					</form>
 				</td>
 				
 			</tr>
 				@endforeach
 				@endif
 			</tbody>
 			
 		</table>
 			
 			
 		</div>
 		
 	</div>
 	
 </div>
 @endsection