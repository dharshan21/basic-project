@extends('layout.app')

@section('content')
    <div class="row">
       
        <div class="col-lg-1">
            <a class="btn btn-primary" href="{{ url('users') }}"> Back</a>
        </div>
    </div>
    <table class="table table-bordered">
        <tr>
            <th>Name:</th>
            <td>{{ $user->name }}</td>
        </tr>
        <tr>
            <th>Email:</th>
            <td>{{ $user->email }}</td>
        </tr>
        <tr>
            <th>Address:</th>
            <td>{{ $user->address }}</td>
        </tr>

    </table>
@endsection