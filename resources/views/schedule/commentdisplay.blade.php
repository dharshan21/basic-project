@foreach($comments as $comment)
    <div class="display-comment" @if($comment->parent_id != null) style="margin-left:40px;" @endif>
        <strong>{{ $comment->add->first_name }}</strong>
        <p>{{ $comment->body }}</p>
        <a href="" id="reply"></a>
       
        <form method="post" action="{{ route('comment.add') }}">
            @csrf
            <div class="form-group">
                <input type="text" name="body" class="form-control" />
                <input type="hidden" name="schedule_id" value="{{ $schedule_id }}" />
                <input type="hidden" name="parent_id" value="{{ $comment->id }}" />
            </div>
            <div class="form-group">
                <input type="submit" class="btn btn-warning" value="Reply" />
            </div>
          
        </form>
        @include('schedule.commentdisplay', ['comments' => $comment->replies])
    </div>
@endforeach