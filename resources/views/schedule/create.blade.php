@extends('layouts.app')

@extends('layouts.header')

@section('content')


<hr>
<form action="{{route('blog_add.store')}}" method="post" enctype="multipart/form-data">
@csrf
<div class="container">

    <div class="form-group">
        <label for="title">Title</label>
        <input type="text" class="form-control"  name="title">
    </div>
     <div class="form-group">
        <label for="body">Text</label>
        <textarea type="text" class="form-control" name="body" rows="10px"></textarea>
    </div>
 
    <div class="form-group">
        <label for="image"> Picture</label>
        <input type="file" class="form-control" name="image">
    </div>

    <button type="submit" class="btn btn-primary">Submit</button>

    </div>
</form>

   @endsection