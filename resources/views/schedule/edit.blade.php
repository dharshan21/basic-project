@extends('layouts.app')



@section('content')


<hr>
<form action="{{route('blog_add.update', $schedule->id)}}" method="post" enctype="multipart/form-data">
@csrf
@method('PATCH')

<div class="container">

    <div class="form-group">
        <label for="title">Title</label>
        <input type="text" class="form-control"  name="title" value="{{ $schedule->title}}">
    </div>
     <div class="form-group">
        <label for="body">Text</label>
        <textarea type="text" class="form-control" name="body">{{ $schedule->body}}"</textarea>
    </div>
 
    <div class="form-group">
        <label for="image"> Picture</label>
        <input type="file" class="form-control" name="image" value="{{ $schedule->image}}">
    </div>


   
    <button type="submit" class="btn btn-primary">Update</button>
    <button type="submit" class="btn btn-primary">Reset</button>

    </div>
</form>

   @endsection