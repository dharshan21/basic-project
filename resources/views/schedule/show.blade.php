@extends('layouts.app')

@extends('layouts.header')


@section('content')
<div class="container">
    <div class="row">
       
        <div class="col-lg-1">
            <a class="btn btn-primary" href="{{ url('blog_add') }}"> Back</a>
        </div>
    </div>

    <table class="table table-bordered">
        <tr>
            <th>Title:</th>
            <td>{{ $schedule->title }}</td>
        </tr>
       
        <tr>
            <th>Text:</th>
            <td>{{ $schedule->body }}</td>

    </table>

 @include('schedule.commentdisplay', ['comments' => $schedule->comments, 'schedule_id' => $schedule->id])
    <div class="card">
        <div class="card-block">
            <form method="POST" action="{{ route('comment.add') }}">
                 @csrf
                <div class="form-group">
                     <textarea name="body" placeholder="your commant here..." class="form-control"></textarea>
                      <input type="hidden" name="schedule_id" value="{{ $schedule->id }}" />

                </div>
                <div class="form-group">
                     <button type="sumbit" class="btn btn-primary">Add Comments</button>
                </div>

               
            </form>
        </div>
    </div>

</div>
@endsection

