@extends('layouts.app')
@extends('layouts.header')
@section('content')
@if(session('success'))
 <div class="alert alert-success" role="alert">
 	{{ session('success')}}
 	
 </div>
 @endif
 <a href="{{ route('blog_add.create')}}" class="btn btn-primary">Create </a>
 <ul class="nav nav-pills">
  <li class="nav-item">
    <a class="nav-link" href="{{'/home'}}">Home</a>
  </li>
</ul>
<div class="container">
 <div class="col-sm-12">
 	<div class="row">
 		<table class="table table-striped">
 			<thead>
 				<th>Id</th>
 				<th>Title</th>
 				<th>Text</th>
 				<th>Picture</th>
 				<th>Pay</th>
 				<th>Show</th>
 				<th>Edit</th>
 				<th>Delete</th>
 			</thead>
 			<tbody>
 				@if($schedules)
 				@foreach($schedules as $schedule)
 				<tr>
 				<td>{{ $schedule->id }}</td>
 				<td>{{ $schedule->title }}</td>
 				<td>{{ $schedule->body }}</td>
 				<td><img src="myfiles/{{ $schedule->image }}" style="border-radius: 50%" width="80px"></td>
 				<td><a class="btn btn-success" href="{{route('stripe.payment')}}">Pay Now  (₹100)</a></td>
 				<td><a class="btn btn-info" href="{{ route('blog_add.show',$schedule->id) }}">Show</a></td>
 				<td><a class="btn btn-primary" href="{{ route('blog_add.edit',$schedule->id) }}">Edit</a></td>
 				<td>
 					<form method="post" action="{{ route('blog_add.destroy',$schedule->id)}}">
 						@csrf
 						@method('DELETE')
 						<button class="btn btn-danger" type="sumbit">Delete</button>
 					</form>
 				</td>
 				
 			</tr>
 				@endforeach
 				@endif
 			</tbody>
 			
 		</table>
 			
 			
 		</div>
 		
 	</div>
 	
 </div>
</div>
 @endsection