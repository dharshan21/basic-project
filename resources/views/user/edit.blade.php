@extends('layouts.app')
@include('layouts.header')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{Auth::user()->name}} My Profile</div>

                <div class="card-body">
                    <form action="{{route('users.update.Profile')}}" method="post" enctype="multipart/form-data">
                        @csrf

                        @method('PUT')

                         <div class="form-group">
                         <label for="name">Picture</label>
                         <input type="file" class="form-control" name="profile_image">
                         </div>
                         <button type="sumbit" class="btn btn-success">Upload Picture</button>
                    </form>
                 

                </div>
            </div>
        </div>
    </div>
</div>

@endsection
