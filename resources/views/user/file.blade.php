@extends('layouts.app')
@include('layouts.header')
@section('content')

@if(session('success'))
 <div class="alert alert-success" role="alert">
    {{ session('success')}}
    
 </div>
 @endif
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Multiple Files Uploads</div>

                <div class="card-body">
                    <form action="{{route('multiple.show')}}"  class="form-horizontal" method="post" enctype="multipart/form-data">
                        @csrf


                         <div class="form-group">
                         <label for="file">File</label>
                         <input type="file" class="form-control-file" name="file[]" id="file" multiple="multiple" accept=".xlsx"/>
                         </div>
                         <button type="sumbit" class="btn btn-success">Upload File</button>
                    </form>
                 

                </div>
            </div>
        </div>
    </div>
</div>

@endsection