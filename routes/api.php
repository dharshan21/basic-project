<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\DeviceApiController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('list', 'DeviceApiController@list');


Route::post('add', 'DeviceApiController@add');

Route::put('update', 'DeviceApiController@update');

Route::delete('delete/{id}', 'DeviceApiController@delete');

Route::get('search/{name}', 'DeviceApiController@search');

Route::post('save', 'DeviceApiController@testData');

Route::post('upload', 'DeviceApiController@upload');