<?php

use Illuminate\Support\Facades\Route;

use App\Http\controllers\UploadController;

use App\Mail\SampleMail;

use App\Http\controllers\LoginAuthController;

use App\Http\controllers\HomeController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view ('welcome');
});

Auth::routes();

Route::get('user/profile', 'HomeController@edit')->name('user.edit.profile');
Route::put('user/profile', 'HomeController@upload')->name('users.update.Profile');

Route::get('/home', 'HomeController@index')->name('home');

Route::get('admin/home', 'HomeController@handleAdmin')->name('admin.home')->middleware('is_admin');

Route::resource("blog",'AddController');

Route::resource("texts_add", 'TextController');

Route::resource("blog_add", "ScheduleController");

Route::post('/comment/store', 'CommentController@store')->name('comment.add');

Route::get('file','FileControllers@index')->name('multiple.file');
Route::post('save', 'FileControllers@show')->name('multiple.show');

Route::get('/stripe-payment','StripeController@paymentIndex')->name('stripe.payment');
Route::post('stripe-payment','StripeController@paymentStatus')->name('stripe.status');

Route::get('/users', 'CacheController@index')->name('users.index');
Route::post('/users/search','CacheController@search')->name('users.search');



// Route::resource('/store', 'ContactController');

// Route::get('contact', 'BasicController@index');

// // Route::view("upload", 'upload');
// Route::get('upload', 'UploadController@changes');
// Route::post('upload', 'UploadController@changesfile')->name('changes.file');


//Laravel Session Login Deatails.......

// Route::get('login', function(){

// 	if (session()->has('user')) {
		
// 		return redirect('profile');
// 	}

// 	return view ('login');

// });

// Route::post('/user', 'LoginAuthController@sessionLogin');

// Route::view("profile", 'profile');


// Route::get('logout', function(){

// 	if (session()->has('user')) {
		
// 		session()->pull('user');
// 	}

// 	return view ('login');

// });

// Route::get('product','ProductControllers@product')->name('product.name');
// // Route::get('products/{id}', 'ProductControllers@product')->where('id', '[0-9]');

// Route::get('products/{name}/{id}', 'ProductControllers@product')->where([

// 	'name' => '[a-z]',
// 	'id' => '[0-9]'
// ]);


// Route::get('place','HelperController@helper')->name('helper.name');
	

// Route::get('/email','MailController@sendEmail');

// Route::get('excel','EmployeeController@add');
// Route::get('explode-excel','EmployeeController@exportIntoExcel');
// Route::get('explode-csv','EmployeeController@exportIntoCSV');

// Route::get('check{name}','StudentController@create')->name('create.route');

// Route::view("user", 'user')->middleware('product');
// Route::view("first", 'first');
// Route::view("noaccess", 'noaccess');
// //group middleware

// Route::group(['middleware' =>['productpage']],function(){

//     // Route::view("user", 'user');
// });

// Route::resource('users','NewController');